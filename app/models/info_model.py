from app import db



class info(db.Model):

    __tablename__="database"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50), nullable=True)
    name = db.Column(db.String(50), nullable=True)
    img = db.Column(db.String(150), nullable=True)
    
    def __str__(self):
        return f"<{self.name} - {self.id}>"

