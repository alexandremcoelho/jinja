from flask import Flask 

def init_app(app: Flask):
    from .api import bp as bp_rotas

    app.register_blueprint(bp_rotas)