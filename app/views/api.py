from flask import Blueprint, request, render_template, redirect
from app.models.info_model import info
from app import db

bp = Blueprint("blueprint", __name__)

@bp.route("/api/post", methods=["GET"])
def retorno():

    return render_template("form.html") 

@bp.route("/api", methods=["GET"])
def show_info():
    data = info.query.all()
    print(data)
    return render_template("cards.html", cards=data)

@bp.route("/api/post", methods=["POST"])
def register_char():
    data = request.form
    print(dict(data))
    obj = info(**data)

    db.session.add(obj)
    db.session.commit()
    return redirect("/api", code=302)

@bp.route("/api/post/<post_id>", methods=["GET"])
def get_post(post_id):
    try:
        post = info.query.filter_by(id=post_id).first_or_404()
        return render_template("cards.html", cards=[post])
    except:
        return render_template('notfound.html')


